import sql from "mssql";
import config from "../config/dbConfig.js";

const fetchData = async (query) => {
  const pool = await sql.connect(config);
  const result = await pool.request().query(query);
  pool.close();
  return result;
};

export { fetchData };
