import { fetchData } from "./fetchData.js";

const getAllProduct = async () => {
  try {
    let result = await fetchData("SELECT * FROM productsdata");
    return { result: result.recordset, code: 200 };
  } catch (e) {
    return { result: e, code: 500 };
  }
};

export default getAllProduct;
