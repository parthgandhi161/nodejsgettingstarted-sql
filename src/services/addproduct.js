import { fetchData } from "./fetchData.js";

const addProduct = async ({ id, name, stock, price }) => {
  if (!id || !name || !stock || !price)
    return {
      result: "Kindly provide valid data",
      code: 400,
    };

  try {
    await fetchData(
      `INSERT INTO productsdata VALUES(${id},'${name}',${stock},${price})`
    );

    return { result: "Product added successfully", code: 201 };
  } catch (e) {
    return { result: e.message, code: 500 };
  }
};

export default addProduct;
