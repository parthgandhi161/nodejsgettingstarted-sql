import { fetchData } from "./fetchData.js";

const getSpecificProduct = async ({ id }) => {
  if (!id) return { result: "Please send valid id", code: 400 };
  try {
    let { recordset } = await fetchData(
      `SELECT * FROM productsdata WHERE id=${id}`
    );
    return { result: recordset, code: 200 };
  } catch (e) {
    return { result: e, code: 500 };
  }
};

export default getSpecificProduct;
