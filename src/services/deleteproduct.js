import { fetchData } from "./fetchData.js";

const deleteProduct = async ({ id }) => {
  if (!id)
    return {
      result: "Please send valid id",
      code: 400,
    };
  try {
    await fetchData(`DELETE FROM productsdata WHERE id=${id}`);
    return { result: "Product deleted successfully", code: 200 };
  } catch (e) {
    return { result: e, code: 500 };
  }
};

export default deleteProduct;
