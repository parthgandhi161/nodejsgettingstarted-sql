import { fetchData } from "./fetchData.js";

const updateProduct = async ({ id, data }) => {
  if (!id || !data)
    return {
      result: "Please provide valid id and data",
      code: 400,
    };
  try {
    let query = [];
    if (data.id) query.push(`id = ${data.id}`);
    if (data.name) query.push(`pname = '${data.name}'`);
    if (data.stock) query.push(`stock = ${data.stock}`);
    if (data.price) query.push(`price = ${data.price}`);

    query = query.join(",");

    let finalQuery = `UPDATE productsdata SET ${query} WHERE id = ${id}`;
    await fetchData(finalQuery);
    return { result: "Product updated successfully", code: 200 };
  } catch (e) {
    return { result: e, code: 500 };
  }
};
export default updateProduct;
