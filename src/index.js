import express from "express";
import product from "./routes/product.js";

const app = express();
app.use(express.json());

const port = process.env.PORT || 3000;

app.use("/product", product);

app.listen(port);
