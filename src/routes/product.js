import express from "express";
import addProduct from "../services/addProduct.js";
import deleteProduct from "../services/deleteProduct.js";
import getAllProduct from "../services/getAllProduct.js";
import getSpecificProduct from "../services/getProductById.js";
import updateProduct from "../services/updateProduct.js";

const router = express.Router();

router.get("/all", async (req, res) => {
  let { code, result } = await getAllProduct();
  res.status(code).send(result);
});

router.get("/", async (req, res) => {
  let { code, result } = await getSpecificProduct(req.query);
  res.status(code).send(result);
});

router.post("/", async (req, res) => {
  let { code, result } = await addProduct(req.body);
  res.status(code).send(result);
});

router.put("/", async (req, res) => {
  let { code, result } = await updateProduct(req.body);
  res.status(code).send(result);
});

router.delete("/", async (req, res) => {
  let { code, result } = await deleteProduct(req.body);
  res.status(code).send(result);
});

export default router;
